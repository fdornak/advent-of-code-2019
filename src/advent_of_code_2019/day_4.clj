(ns advent-of-code-2019.day-4)

(defn digits [n]
  (->> n
       (iterate #(quot % 10))
       (take-while pos?)
       (mapv #(mod % 10))
       rseq))

(defn never-decreasing? [number]
  (loop [num (digits number)
         still-decreasing? true]
    (if (= (count num) 1)
      still-decreasing?
      (recur (rest num) (if (>= (second num) (first num)) (and still-decreasing? true) false)))))

(defn has-one-double? [number]
  (some #(= %1 2) (vals (frequencies (digits number))))
  )

(defn has-two-adjacent-digits? [number]
  (<= (loop [num (digits number)
            acc 0]
       (if (= (count num) 1)
         acc
         (recur (rest num) (if (= (first num) (second num)) acc (inc acc))))) (- (count (digits number)) 2)))

(defn count-password-options-first [start end]
  (count (->>
           (range start end)
           (filter has-two-adjacent-digits?)
           (filter never-decreasing?)
           )))

(defn count-password-options-second [start end]
  (count (->>
     (range start end)
     (filter has-one-double?)
     (filter never-decreasing?)
     )))

(count-password-options-first 156218 652527)
(count-password-options-second 156218 652527)