(ns advent-of-code-2019.day-1)

(defn fuel-requirement [mass]
  (- (int (Math/floor (/ mass 3))) 2))

(defn- read-and-parse-string [filename]
  (clojure.string/split-lines (slurp (.getFile (clojure.java.io/resource filename)))))

(defn fuel-requirement-part-2 [mass]
  (loop [m mass
         acc 0]
    (if (neg-int? (fuel-requirement m))
      acc
      (recur (fuel-requirement m) (+ acc (fuel-requirement m))))))

(reduce + (->>
            (read-and-parse-string "day-1-modules.txt")
            (map read-string)
            (map fuel-requirement)))

(reduce + (->>
            (read-and-parse-string "day-1-modules.txt")
            (map read-string)
            (map fuel-requirement-part-2)))

