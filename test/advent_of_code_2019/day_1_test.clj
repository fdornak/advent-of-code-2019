(ns advent-of-code-2019.day-1-test
  (:require [clojure.test :refer :all]
            [advent-of-code-2019.day-1 :refer :all]))

(deftest fuel-requirement-test
  (testing "First"
    (is (= (fuel-requirement 12) 2)))
  (testing "Second"
    (is (= (fuel-requirement 14) 2)))
  (testing "Third"
    (is (= (fuel-requirement 1969) 654)))
  (testing "Fourth"
    (is (= (fuel-requirement 100756) 33583))))

(deftest fuel-requirement-part-2-test
  (testing "First"
    (is (= (fuel-requirement-part-2 14) 2)))
  (testing "Second"
    (is (= (fuel-requirement-part-2 1969) 966)))
  (testing "Third"
    (is (= (fuel-requirement-part-2 100756) 50346))))
